var prikaziVzdevek='';
var currentRoom='';
function divElementEnostavniTekst(sporocilo) {
  sporocilo = htmlEncode(sporocilo);
  return $('<div style="font-weight: bold"></div>').append(sporocilo);//text(sporocilo);
}


function divElementHtmlTekst(sporocilo) {
  sporocilo=htmlEncode(sporocilo);
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
function htmlEncode( input) {
    return String(input)
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
}


function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo; var uporabnik,zasSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sporocilo = htmlEncode(sporocilo);
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) { 
      if(sistemskoSporocilo !='Neznan ukaz.'){//dobil sem zasebno sporocilo
          
          if(sistemskoSporocilo.length>0){
            uporabnik=sistemskoSporocilo.shift();
            uporabnik=uporabnik.substring(1,uporabnik.length-1);
          }
          
          
          if(sistemskoSporocilo.length>0){
            zasSporocilo=sistemskoSporocilo.shift();
            zasSporocilo=zasSporocilo.substring(1,zasSporocilo.length-1);
            zasSporocilo = htmlEncode(zasSporocilo);
            zasSporocilo = filterBadWords(zasSporocilo);
            klepetApp.zasebnoSporocilo($('#kanal').text(),uporabnik,zasSporocilo);
          //$('#sporocila').append(divElementEnostavniTekst(zasSporocilo));
          $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
          }else{
            $('#sporocila').append(divElementHtmlTekst('Neznan ukaz.'));
          }
          
      }
     $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = htmlEncode(sporocilo);
    sporocilo = filterBadWords(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}
function filterBadWords(text){
  var textArray=text.split(' '); var word;
  var regEx; var test; 
  var specialChars=['\\','^','$','.','|','?','*','+','(',')','[','{'];
  var stop=false;
  for(var i in textArray){
     word = textArray[i].split(/([_\W])/);
    for(var k in word){
      stop=false;
      for(var j in specialChars){
        test = word[k].indexOf(specialChars[j]);
        
        if(test>-1){
          stop=true;
          break;
        }
      }
      if(stop){
        continue;;
      }
      regEx = new RegExp('\\b'+word[k]+'\\b','gi');
      if( regEx.test(swearWords)){
        text=text.replace(regEx,zamenjaj(word[k]));
      }
    }
    
      
    
  }
  
  
  return text;
}
function zamenjaj (match){
   var censored='';
   for(var i=0;i<match.length;i++){
      censored+='*';
    }
    return censored;
  
}

  
var socket = io.connect();
var swearWords='';
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  $.get("swearWords.txt", function(odgovor) {
       swearWords  = odgovor;
});
 

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      prikaziVzdevek=rezultat.vzdevek;
      $('#kanal').text(prikaziVzdevek+' @ '+currentRoom);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    //test naloga 2_2  spremeni kanal
    currentRoom=rezultat.kanal;
    $('#kanal').text(prikaziVzdevek+' @ '+currentRoom);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').append(sporocilo.besedilo);
   
    $('#sporocila').append(novElement);
  });
  
  socket.on('zasebno', function(sporocilo) {
      var novElement = $('<div style="font-weight: bold"></div>').append(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

 socket.on('uporabniki',function(uporabniki){
   $('#seznam-uporabnikov').empty();
  for(var uporabnik in uporabniki){
     if(uporabniki[uporabnik]!=null){
       $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[uporabnik]));
     }
    
  }
 });
  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  //uporabniki
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});