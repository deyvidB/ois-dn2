var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};
//zasebno sporocilo
Klepet.prototype.zasebnoSporocilo = function(kanal, uporabnik, besedilo){
  var zasebnoSporocilo = {
    kanal: kanal,
    uporabnik: uporabnik,
    besedilo: besedilo
  };
  this.socket.emit('zasebno', zasebnoSporocilo);
}

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.match(/(?:[^\s"]+|"[^"]*")+/g); //ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var uporabnik='';
  var zasSporocilo='';
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev': // "OIS" "Porto Katsiki"
      besede.shift();
      var channel=besede.shift(); // "OIS"
      var kanal=besede.join(' ');// =besede.join((' '))
      if(kanal.charAt(0)=='"' || besede.length>0){
         kanal = channel+' '+kanal;
      }else {
        kanal = channel+kanal;
      }
         this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      if(besede.length<3){
        sporocilo = 'Neznan ukaz.';
      }else if(besede[1].indexOf('"') === 0 && besede[1].substring(1,besede[1].length).indexOf('"') > -1 &&
      besede[2].indexOf('"') === 0 && besede[2].substring(1,besede[2].length).indexOf('"') > -1){
        besede.shift();
        sporocilo=besede;
      }else{
        sporocilo = 'Neznan ukaz.';
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};