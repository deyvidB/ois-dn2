var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var trenutniUporabniki = [];
var uporabnikiKanala= {};
var curChanel='Skedenj';
var prevChanel='Skedenj';
var kanalGeslo = {};
var zascKanali = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik); //poslusa websocket protokol
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    //pridruzitevKanaluNadgradnja(socket,'Skedenj');
    pridruzitevKanalu(socket,'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    
   socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', uporabnikiKanala[trenutniKanal[socket.id]]);
   socket.emit('uporabniki', uporabnikiKanala[trenutniKanal[socket.id]]);
  
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
  
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal}); 
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  var uporabnikiNaKanalu = io.sockets.clients(kanal); //kdo je na kanalu
  
  curChanel=kanal;
  trenutniUporabniki=[];
  trenutniUporabniki.push(vzdevkiGledeNaSocket[socket.id]);
  uporabnikiKanala[trenutniKanal[socket.id]]=trenutniUporabniki;
  
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        trenutniUporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
 
  uporabnikiKanala[trenutniKanal[socket.id]] = trenutniUporabniki;
  
  socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki',uporabnikiKanala[trenutniKanal[socket.id]]);
  socket.emit('uporabniki',uporabnikiKanala[trenutniKanal[socket.id]]);
  }
}
function pridruzitevKanaluNadgradnja(socket,kanal){
  //preveri ali je zaščiten kanal
  var text = kanal.match(/(?:[^\s"]+|"[^"]*")+/g);
  if(kanal.charAt(0) == '"' && text.length>1){   //
    //pridruzitevKanalu(socket,kanal);
    kanal = kanal.match(/(?:[^\s"]+|"[^"]*")+/g); //split
    var channel = kanal.shift(); //"OIS"
    var geslo = kanal.shift();
    channel = channel.substring(1,channel.length-1); // OIS
    geslo = geslo.substring(1,geslo.length-1); //Porto katsiki
    var kanalObstaja=false;
    for(var i in trenutniKanal){
        if(channel == trenutniKanal[i]){ // kanal že obsaja
          kanalObstaja=true;
          break;
        }
      }
      if(kanalObstaja){
          if(kanalGeslo[channel] === undefined){
            
             socket.emit('sporocilo', {besedilo: 'Izbrani kanal '+channel+' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.'});
          }
          else if(geslo == kanalGeslo[channel]){
            socket.leave(trenutniKanal[socket.id]);
            updateUsers(socket);
            pridruzitevKanalu(socket,channel);
          }else{
             socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal '+ channel +' ni bilo uspešno, ker je geslo napačno!'});
          }
      }else{
        kanalGeslo[channel] = geslo;
        socket.leave(trenutniKanal[socket.id]);
        updateUsers(socket);
        pridruzitevKanalu(socket,channel);
        zascKanali.push(channel);
      }
    
  }else{
    //če ni zaščiten normalno nadaljujemo
    if(zascKanali.indexOf(kanal)<0 ){
      socket.leave(trenutniKanal[socket.id]);
      updateUsers(socket);
      pridruzitevKanalu(socket,kanal);
    }else{
       socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal '+ kanal +' ni bilo uspešno, ker je geslo napačno!'});
    }
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        
        trenutniUporabniki = uporabnikiKanala[trenutniKanal[socket.id]];
        trenutniUporabniki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        prejsnjiVzdevekIndeks=trenutniUporabniki.indexOf(prejsnjiVzdevek);
        delete trenutniUporabniki[prejsnjiVzdevekIndeks];
        uporabnikiKanala[trenutniKanal[socket.id]] = trenutniUporabniki;
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        //update channel list
        socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', uporabnikiKanala[trenutniKanal[socket.id]]);
        socket.emit('uporabniki', uporabnikiKanala[trenutniKanal[socket.id]]);
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}
//začetek dela na veji Naloga 2.6
function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    var ind=sporocilo.kanal.search("@ ");
    var input=sporocilo.kanal.substring(ind+2,sporocilo.kanal.length);
    
    socket.broadcast.to(input).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + replaceEmoticons(sporocilo.besedilo)
    });
    socket.emit('sporocilo',{
      besedilo: replaceEmoticons(sporocilo.besedilo)});

  });
  
  socket.on('zasebno',function(zasebnoSporocilo) {
     var value=zasebnoSporocilo.uporabnik;
     for(var i in vzdevkiGledeNaSocket){
       if(vzdevkiGledeNaSocket[i] == value && vzdevkiGledeNaSocket[i] != vzdevkiGledeNaSocket[socket.id]){
         value=i;
         io.sockets.socket(value).emit('zasebno', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + replaceEmoticons(zasebnoSporocilo.besedilo) });
      //poslji besedilo trenutnemu uporabniku
      socket.emit('sporocilo',{
      besedilo: '(zasebno za '+zasebnoSporocilo.uporabnik+'): '+replaceEmoticons(zasebnoSporocilo.besedilo)
    });
         }
     }
     if(value==zasebnoSporocilo.uporabnik){
         
         io.sockets.socket(socket.id).emit('zasebno', {
      besedilo: 'Sporočila '+ replaceEmoticons(zasebnoSporocilo.besedilo)+' uporabniku z vzdevkom '+ zasebnoSporocilo.uporabnik+ ' ni bilo mogoče posredovati.'  });
       }
  });
}


  
function replaceEmoticons(text) {
      var emoticons = {
        ';)' : 'wink.png',
        ':)'  : 'smiley.png',
        '(y)'  : 'like.png',
        ':*'  : 'kiss.png',
        ':('  : 'sad.png'
      }, url = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/";
      return text.replace(/[:;(][y]?[*)(]/g, function (match) {
        if(emoticons[match] !== undefined){
          return '<img src="'+url+emoticons[match]+'"></img>';
        }
        return match;
      });
    }

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {

    pridruzitevKanaluNadgradnja(socket,kanal.novKanal);
  });
}

function updateUsers(socket){
  prevChanel = trenutniKanal[socket.id];
  trenutniUporabniki = uporabnikiKanala[prevChanel];
  var vzdevekIndeks=trenutniUporabniki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    var temp=[];
    temp.push(trenutniUporabniki[vzdevekIndeks]);
  
    socket.emit('uporabniki', temp);
    trenutniUporabniki.splice(vzdevekIndeks,1);
    
    uporabnikiKanala[prevChanel]=trenutniUporabniki;
    socket.broadcast.to(prevChanel).emit('uporabniki', uporabnikiKanala[prevChanel]);//uporabnikiKanala[curChanel]);

}
function obdelajOdjavoUporabnika(socket) {
  socket.on('disconnect', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    trenutniUporabniki = uporabnikiKanala[trenutniKanal[socket.id]];
    vzdevekIndeks=trenutniUporabniki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete trenutniUporabniki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    uporabnikiKanala[trenutniKanal[socket.id]] = trenutniUporabniki;
    socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', uporabnikiKanala[trenutniKanal[socket.id]]);
    
  });
}